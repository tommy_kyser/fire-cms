<?php
head( 'FOOBAX' );
global $db;
$data         = $db->get( 'home' );
$data->header = 'test';
$data->save();
//echo $data->header;

?>
	<h1 class="test"></h1>
	<button>SET field FOO to BAR</button>
	<script>
		$(document).ready(function () {
			$.ajax(
				{
					url: "?FIRE=true&index=home&field=FOO&method=get", success: function (result) {
						$('.test').html(result);
					}
				});
			$("button").click(function () {
				var cars = ["Saab", "Volvo", "BMW"];
				var uri = "?FIRE=true&index=home&field=FOO&method=set&value="+cars;
				$.ajax({
					url: uri, success: function (result) {
						$('.test').html(result);
					}
				});
			});

		});
	</script>
<?php
bind_view( 'home.php' );
footer();