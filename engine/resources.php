<?php
// GLOBAL RESOURCES
// Loaded in head object
$global_header = new \Kyser\loader();
$global_header->register('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css','css');
$global_header->register('jquery', 'https://code.jquery.com/jquery-3.3.1.min.js','js');
$global_header->register('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js','js');
$global_header->register('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js','js');
