<?php
$current_page = $_SERVER['REQUEST_URI'];
if($current_page === '/index' || $current_page === '/index.php' || $current_page === '/index.html' || $current_page === '/home' || $current_page === '/'){
	require_once ABSPATH . 'actions/' . 'index.php';
}

function route($action, $file){
	$listen       = $_REQUEST['action'];
	if($listen === $action){
		require_once ABSPATH . 'actions/' . $file;
	}
}
