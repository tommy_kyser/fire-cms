<?php
namespace Kyser;


class Fire {
	private $access;
	public function __construct($index) {
		global $db;
		$this->access = $db->get($index);
	}
	public function get_field($field){
		return $this->access->$field;
	}
	public function set_field($field, $value){
		 $this->access->$field = $value;
		 $this->access->save();

	}


}