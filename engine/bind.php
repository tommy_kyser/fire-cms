<?php
function bind_controller($controller){
	require_once ABSPATH . 'controllers/' . $controller;
}
function bind_view($view){
	require_once ABSPATH . 'views/' . $view;
}