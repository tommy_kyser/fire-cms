<?php

namespace Kyser;

class loader {
	private $styles_head;
	private $styles_footer;
	private $js_head;
	private $js_footer;

	public function register( $name, $resource, $type, $order = 'head' ) {
		if ( $order === 'head' ) {
			if ( $type === 'css' ) {
				$this->styles_head[$name] = $resource;
			}
			if ( $type === 'js' ) {
				$this->js_head[$name] = $resource;
			}
		}
		if ( $order === 'footer' ) {
			if ( $type === 'css' ) {
				$this->styles_footer[$name] = $resource;
			}
			if ( $type === 'js' ) {
				$this->js_footer[$name] = $resource;
			}
		}
	}

	public function print_head() {
		$open_tag  = '<link rel="stylesheet" href="';
		$close_tag = '">';
		$styles    = $this->styles_head;
		foreach ( $styles as $style => $sheet ) {
			echo $open_tag . $sheet . $close_tag;
		}
		$open  = '<script src="';
		$close = '"></script>';
		$obj   = $this->js_head;
		foreach ( $obj as $key => $file ) {
			echo $open . $file . $close;
		}

	}

	public function print_footer() {

		$open_tag  = '<link rel="stylesheet" href="';
		$close_tag = '">';
		$styles    = $this->styles_footer;
		foreach ( $styles as $style => $sheet ) {
			echo $open_tag . $sheet . $close_tag;
		}

		$open  = '<script src="';
		$close = '"></script>';
		$obj   = $this->js_footer;
		foreach ( $obj as $key => $file ) {
			echo $open . $file . $close;
		}
	}

}



